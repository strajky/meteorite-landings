package com.example.strajky.meteoritelandings;

import com.socrata.android.client.SodaEntity;
import com.socrata.android.client.SodaField;
import com.socrata.android.soql.datatypes.Location;

@SodaEntity
public class Meteorite implements Comparable <Meteorite>  {

    @SodaField("name")
    public String name;

    @SodaField("id")
    private String id;

    @SodaField("nametype")
    private String nametype;

    @SodaField("recclass")
    private String recclass;

    @SodaField("mass")
    private String mass;

    @SodaField("fall")
    private String fall;

    @SodaField("year")
    private String year;

    @SodaField("reclat")
    private String reclat;

    @SodaField("reclong")
    private String reclong;

    @SodaField("geolocation_address")
    private String geolocation_address;

    @SodaField("geolocation_zip")
    private String geolocation_zip;

    @SodaField("geolocation_city")
    private String geolocation_city;

    @SodaField("geolocation")
    private Location geolocation;

    @SodaField("geolocation_state")
    private String geolocation_state;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getNametype() {
        return nametype;
    }

    public String getRecclass() {
        return recclass;
    }

    public String getMass() {
        return mass;
    }

    public String getFall() {
        return fall;
    }

    public String getYear() {
        return year;
    }

    public String getReclat() {
        return reclat;
    }

    public String getReclong() {
        return reclong;
    }

    public String getGeolocation_address() {
        return geolocation_address;
    }

    public String getGeolocation_zip() {
        return geolocation_zip;
    }

    public String getGeolocation_city() {
        return geolocation_city;
    }

    public String getGeolocation_state() {
        return geolocation_state;
    }

    public void setId(Integer id) {
        this.id = id.toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public void setReclat(String reclat) {
        this.reclat = reclat;
    }

    public void setReclong(String reclong) {
        this.reclong = reclong;
    }

    @Override
    public int compareTo(Meteorite listItem) {
        if (this.mass != null)
            return Double.valueOf(listItem.getMass()).compareTo(Double.valueOf(this.mass));
        else
            throw new IllegalArgumentException();
    }
}


