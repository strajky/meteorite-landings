package com.example.strajky.meteoritelandings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.strajky.meteoritelandings.SQLite.MySQLiteDAO;
import com.socrata.android.client.Callback;
import com.socrata.android.client.Consumer;
import com.socrata.android.client.Response;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "MainActivity";

    public List<Meteorite> meteorites = null;
    public List<Meteorite> meteoriteList = null;
    private  ListView listView;
    public  MySQLiteDAO sqLiteDAO;
    private MeteoriteAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.meteorite_listview);

        sqLiteDAO = new MySQLiteDAO(this);
        sqLiteDAO.open();

        /* SHOW ALL METEORITES IN LIST VIEW ON SCREEN IF TABLE EXISTS*/
        if (sqLiteDAO.sizeOfTable() > 0) {
            setMeteoriteView();
        } else {
            getMeteorites();
        }

        //Log.i(LOG_TAG, String.valueOf(sqLiteDAO.sizeOfTable()));

        /* RUN DOWNLOAD SERVICE - EVERY 24 HOURTS GET DATA FROM NASA SERVER */
        Thread t = new Thread(){
            public void run(){
                Intent serviceIntent = new Intent(getApplicationContext(), DownloadService.class);
                startService(serviceIntent);
            }
        };
        t.start();

        /* CLICK LISTENER - SHOW METEORITE ON MAP */
        showMeteoriteOnMap();

    }

    /* DOWNLOAD METEORITES LANDINGS - SINCE 2011 */
    public void getMeteorites() {
        Consumer consumer = new Consumer("data.nasa.gov", "qL2EYi8fMEFmakHH3dfDhQjcs");

        /* USING SODA SKD FOR GET DATA FROM NASA */
        consumer.getObjects("y77d-th95", "select * where year >= '2011-01-01T00:00:00.000'", Meteorite.class, new Callback<List<Meteorite>>() {
            @Override
            public void onResults(Response<List<Meteorite>> response) {
                meteorites = response.getEntity();

                /* DELETE AND CREATE TABLE METEORITE */
                sqLiteDAO.initTable();

                /* FILL METEORITE TABLE */
                for (Meteorite meteorite: meteorites) {
                    Integer id = Integer.valueOf(meteorite.getId());
                    String name = meteorite.getName();
                    String year = meteorite.getYear();
                    String mass = meteorite.getMass();
                    String reclat = meteorite.getReclat();
                    String reclong = meteorite.getReclong();

                    if (year == null) year = "0000";
                    if (mass == null) mass = "0";
                    if (reclat == null) reclat = "0";
                    if (reclong == null) reclong = "0";

                    sqLiteDAO.createMeteorite(id, name, year, mass, reclat, reclong);
                }
                //Log.i(LOG_TAG, "v getMeteorites -> " + String.valueOf(sqLiteDAO.sizeOfTable()));

                /* SHOW ALL METEORITES IN LIST VIEW ON SCREEN */
                setMeteoriteView();
            }
        });
    }

    private void setMeteoriteView() {
        meteoriteList = sqLiteDAO.getAllMeteorites();

        Collections.sort(meteoriteList);   // Sort list by MASS value

        adapter = new MeteoriteAdapter(getApplicationContext(), R.layout.meteorite_list_item, meteoriteList);
        listView.setAdapter(adapter);

        this.setTitle("Meteorites (" + String.valueOf(sqLiteDAO.sizeOfTable()) + "  landed)");
    }

    private void showMeteoriteOnMap() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Meteorite meteorite = adapter.getItem(position);

                String reclat = meteorite.getReclat();
                String reclong = meteorite.getReclong();

                //Log.i(LOG_TAG, "reclat:" + reclat + ", reclong:" + reclong);

                if ((reclat != null) && (reclat != null)) {
                    showMeteoriteOnMap(reclat, reclong, meteorite.getName());

                } else {
                    Toast.makeText(getApplicationContext(), "Empty location", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    private void showMeteoriteOnMap(String reclat, String reclong, String name) {
        String uriBegin = "geo:" + reclat + "," + reclong;
        String query = reclat + "," + reclong + "(" + name + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery;
        Uri uri = Uri.parse(uriString);

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
