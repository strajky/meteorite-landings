package com.example.strajky.meteoritelandings.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_METEORITES = "meteorites";
    public static final String COLUMN_ID = "meteorite_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_MASS = "mass";
    public static final String COLUMN_RECLAT = "reclat";
    public static final String COLUMN_RECLONG = "reclong";

    private static final String DATABASE_NAME = "meteorites.db";
    public static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_METEORITES + "("
            + COLUMN_ID + " integer primary key,"
            + COLUMN_NAME + " text not null,"
            + COLUMN_YEAR + " text not null,"
            + COLUMN_MASS + " text not null,"
            + COLUMN_RECLAT + " text not null,"
            + COLUMN_RECLONG + " text not null"
            + ");";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_METEORITES);
        onCreate(db);
    }

    public void deleteCreateTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_METEORITES);
        db.execSQL(DATABASE_CREATE);
    }
}