package com.example.strajky.meteoritelandings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class MeteoriteAdapter extends ArrayAdapter<Meteorite> {
    private final String LOG_TAG = "MeteoriteAdapter";

    private Context con;
    private int id;
    private List<Meteorite> items;

    public MeteoriteAdapter(Context context, int id, List<Meteorite> items) {
        super(context, id, items);

        this.con = context;
        this.id = id;
        this.items = items;
    }

    public Meteorite getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater view = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = view.inflate(id, null);
        }

        Meteorite item = getItem(position);

        if (item != null) {
            TextView itemName = (TextView) convertView.findViewById(R.id.itemname);
            TextView itemInfo = (TextView) convertView.findViewById(R.id.iteminfo);

            if (itemName != null)
                itemName.setText(item.getName());

            if (itemInfo != null) {
                String year = item.getYear();
                String mass = item.getMass();

                year = year.substring(0,4);

                if (year == null)
                    year = "no year";
                if (mass == null)
                    mass = "0";

                itemInfo.setText(year + "y, " + mass + "g");
            }
        }

        return convertView;
    }
}