package com.example.strajky.meteoritelandings;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.SystemClock;

import com.example.strajky.meteoritelandings.SQLite.MySQLiteDAO;
import com.socrata.android.client.Callback;
import com.socrata.android.client.Consumer;
import com.socrata.android.client.Response;

import java.util.List;


public class DownloadService extends IntentService {
    private static final String LOG_TAG = "DownloadService";

    private  MySQLiteDAO sqLiteDAO;
    private Consumer consumer;

    public DownloadService() {
        super("DownloadService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sqLiteDAO = new MySQLiteDAO(this);
        sqLiteDAO.open();

        consumer = new Consumer("data.nasa.gov", "qL2EYi8fMEFmakHH3dfDhQjcs");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SystemClock.sleep(86400000);    // Re-load data from NASA server every 24 hours

        /* CHECK INTERNET CONNECTION */
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null && (cm.getActiveNetworkInfo().isAvailable()) && (cm.getActiveNetworkInfo().isConnected())) {

             /* USING SODA SKD FOR GET DATA FROM NASA - METEORITE SINCE 2011*/
            consumer.getObjects("y77d-th95", "select * where year >= '2011-01-01T00:00:00.000'", Meteorite.class, new Callback<List<Meteorite>>() {
                @Override
                public void onResults(Response<List<Meteorite>> response) {
                    List<Meteorite> meteorites = response.getEntity();

                    /* DELETE AND CREATE TABLE METEORITE */
                    sqLiteDAO.initTable();
                    //Log.i(LOG_TAG, String.valueOf(sqLiteDAO.sizeOfTable()));

                    /* FILL METEORITE TABLE */
                    for (Meteorite meteorite : meteorites) {
                        Integer id = Integer.valueOf(meteorite.getId());
                        String name = meteorite.getName();
                        String year = meteorite.getYear();
                        String mass = meteorite.getMass();
                        String reclat = meteorite.getReclat();
                        String reclong = meteorite.getReclong();

                        if (year == null) year = "0000";
                        if (mass == null) mass = "0";
                        if (reclat == null) reclat = "0";
                        if (reclong == null) reclong = "0";

                        sqLiteDAO.createMeteorite(id, name, year, mass, reclat, reclong);
                    }
                    //Log.i(LOG_TAG, String.valueOf(sqLiteDAO.sizeOfTable()));
                }
            });
        }

        Intent serviceIntent = new Intent(this, DownloadService.class);
        startService(serviceIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent,flags,startId);

        return START_STICKY;
    }
}