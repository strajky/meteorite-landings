package com.example.strajky.meteoritelandings.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.strajky.meteoritelandings.Meteorite;

import java.util.ArrayList;
import java.util.List;

import static android.database.DatabaseUtils.queryNumEntries;

public class MySQLiteDAO {
    private final static String LOG_TAG = "MySQLiteDAO";

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = {
            MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_NAME,
            MySQLiteHelper.COLUMN_YEAR,
            MySQLiteHelper.COLUMN_MASS,
            MySQLiteHelper.COLUMN_RECLAT,
            MySQLiteHelper.COLUMN_RECLONG};

    public MySQLiteDAO(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createMeteorite( Integer id, String name, String year, String mass, String reclat, String reclong) {
        ContentValues values = new ContentValues();

        //Log.i(LOG_TAG, name + " " + year + " " + mass);

        values.put(MySQLiteHelper.COLUMN_ID, id);
        values.put(MySQLiteHelper.COLUMN_NAME, name);
        values.put(MySQLiteHelper.COLUMN_YEAR, year);
        values.put(MySQLiteHelper.COLUMN_MASS, mass);
        values.put(MySQLiteHelper.COLUMN_RECLAT, reclat);
        values.put(MySQLiteHelper.COLUMN_RECLONG, reclong);

        long insertId = database.insert(MySQLiteHelper.TABLE_METEORITES, null, values);

        Cursor cursor = database.query(MySQLiteHelper.TABLE_METEORITES, allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();

        cursor.close();
    }

    public List<Meteorite> getAllMeteorites() {
        List<Meteorite> meteorites = new ArrayList<Meteorite>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_METEORITES, allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Meteorite meteorite = cursorToMeteorite(cursor);
            //Log.i(LOG_TAG, meteorite.getName() + " " + meteorite.getYear() + " " + meteorite.getMass());
            meteorites.add(meteorite);
            cursor.moveToNext();
        }

        cursor.close();
        return meteorites;
    }

    private Meteorite cursorToMeteorite(Cursor cursor) {
        Meteorite comment = new Meteorite();

        comment.setId(cursor.getInt(0));
        comment.setName(cursor.getString(1));
        comment.setYear(cursor.getString(2));
        comment.setMass(cursor.getString(3));
        comment.setReclat(cursor.getString(4));
        comment.setReclong(cursor.getString(5));
        return comment;
    }

    public void initTable() {
        dbHelper.deleteCreateTable(database);
    }

    public Long sizeOfTable() {
        return queryNumEntries(database, "meteorites");
    }
}
